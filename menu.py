from os import system
from students import Student as s
from admin import Admin as a
from center import Center as c

def student_menu():
    system("clear")
    while(True):
        
        print("\nWelcome to Student Menu")
        print("\n0.exit\n1.Register \n2.Sign In\nEnter Choice: ")
        ch=int(input())
        if ch==1:
          s().register()
        elif ch==2:
          print("Enter User Name: ")
          uname=input()
          print("Enter password: ")
          passwd=input()
          while(True):

            if(s().validate(uname,passwd)):
                   
                    print("\n0.Log Out\n1.Course List \n2.Center List\n3.Give Preference\n4.check allocated center\n5.update payment detail")
                    print("\nEnter choice: ")
                    choice=int(input())
                    if choice==1:
                         s().courseList()
                    elif choice==2:
                        s().centerList()
                    elif choice==3:
                        s().setPreference(uname)
                    elif choice==4:
                        s().checkAllocatedCenter(uname)
                    elif choice==5:
                        s().updatePaymentDetail(uname)
                    elif choice==0:
                        break
                    else:
                        print("Enter Correct choice")
                    
            else:
                print("\nInvalid Credential\n")
                break
                    
        elif ch==0:
            break
        else:
            print("Invalid Input")
    

def admin_menu():
    system("clear")
    print("Enter User Name: ")
    uname=input()
    print("Enter password: ")
    passwd=input()
    if(a().validate(uname,passwd)):
        while(True):
            print("\nWelcome to Admin Menu")
            print('''\n0.Log Out
                     \n1.List courses & eligibilities 
                     \n2.List centers & capacities 
                     \n3.List students
                     \n4.Update student ranks
                     \n5.Allocate centers (Round 1)
                     \n6.Allocate centers (Round 2)
                     \n7.List allocated students
                     \n8.List paid students
                     \n9.List reported (at center) students
                     \n10.Generate PRN
                     \n11.List admitted students (with PRN) for given center 
                     \nEnter Choice: ''')
            ch=int(input())
            if ch==0:
                break
            elif ch==1:
                a().course_eligi()
            elif ch==2:
                a().center_cap()
            elif ch==3:
                a().student_list()
            elif ch==4:
                a().update_rank()
            elif ch==5:
                 a().allocate_center1()
            elif ch==6:
                a().allocate_center2()
            elif ch==7:
                a().allocation_list()
            elif ch==8:
                a().paid_list()
            elif ch==9:
                a().reported_students()
            elif ch==10:
                a().allocate_prn()
            elif ch==11:
                a().admitted_Student_List()
            else:
                print("Enter Correct choice")
    else:
        print("\n..........Invalid credentials..........\n")


def center_menu():
    system("clear")
    print("Enter User Name: ")
    uname=str.upper(input())
    print("Enter password: ")
    passwd=input()
    if(c().validate(uname,passwd)):
       
        while(True):
             print("\nWelcome to center Cordinator Menu")
             print('''\n0.Log out
                       \n1.List Courses
                       \n2.List Students
                       \n3.Update Report Status
                       \n4.List of admitted student
                       \nEnter Choice:''')
             ch=int(input())
             if ch==0:
                break
             elif ch==1:
                c().list_course(uname)
             elif ch==2:
                c().list_student(uname)
             elif ch==3:
                c().update_report_status(uname)
             elif ch==4:
                c().admitted_Student_List(uname)
             else:
                 print("Enter Correct choice")
    else:
        print("Entered Wrong credentials")

