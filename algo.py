import fileProcess

fieldname=[['fno','name','A','B','C','degree','percentage','preference','course','cid','payment','cReport','prn'],
['id','name','fees','section'],['name','degree','percentage'],['degree'],['id','name','address','coordinator','password'],
['fno','pno','course','cid'],['cid','cname','capacity','fcapacity']]
path=["./Modified_Data_Files/students.csv","./Modified_Data_Files/courses.csv","./Modified_Data_Files/eligibilities.csv",
              "./Modified_Data_Files/degrees.txt","./Modified_Data_Files/centers.csv", "./Modified_Data_Files/preferences.csv",
              "./Modified_Data_Files/capacities.csv" ]
studentData=fileProcess.getData(path[0],fieldname[0])
courseData=fileProcess.getData(path[1],fieldname[1])
eligibleData=fileProcess.getData(path[2],fieldname[2])
degreesData=fileProcess.getData(path[3],fieldname[3])
centersData=fileProcess.getData(path[4],fieldname[4])
preferenceData=fileProcess.getData(path[5],fieldname[5])
capacityData=fileProcess.getData(path[6],fieldname[6])

def getPreference(fno):
    p_data={}
    for s in studentData:
        temp=[]
        for p in preferenceData:
            if s['fno']==p['fno']:
                temp.append([p['course'],p['cid'],p['pno']])
        if len(temp)!=0:
            p_data[s['fno']]=temp
    return p_data.get(fno,-1)
    



def getCourseList(rank):
   l={}
   sections=['A','B','C']
   for i in sections:
       t=[]
       for r in courseData:
           if r["section"]==i:
               t.append(r['name'])
       l[i]=t
   return l.get(rank)


def incrementor(capacityData,cid,cname,pno,stud):
    for cap in capacityData:
        if cap['cid'] == cid and cap['cname'] == cname:
            if int(cap['fcapacity'])<int(cap["capacity"]):
                cap['fcapacity'] = str(int(cap['fcapacity']) +1)
                stud['course'] = cname
                stud['cid'] = cid
                stud['preference'] = pno



def algo1(studentData):
    sections=['A','B','C']
    count=0
    for cycle in range(1,11):
        for rank in sections:
            studentData=sorted(studentData,key=lambda x:int(x[rank]))
            for stud in studentData:
                if stud[rank]!='-1' and stud['course']=="NA" and stud['preference']=='0':
                        preferance=getPreference(stud['fno'])
                        course=getCourseList(rank)
                        if preferance!=-1:
                            if preferance[0][0] in course and preferance[0][2]==str(cycle) :
                                cname,cid,pno = preferance[0]
                                incrementor(capacityData,cid,cname,pno,stud)
                                count+=1
    fileProcess.writeData("./Modified_Data_Files/capacities.csv",capacityData,fieldname[6])
    #return studentData#sorted(studentData,key=lambda x:int(x['fno']))
    
def modifyStudent_Cap(row):
        for i in capacityData:
            if i['cid']==row['cid'] and int(i['fcapacity'])>0:
                i['fcapacity']=str(int(i['fcapacity']) -1)
        fileProcess.writeData("./Modified_Data_Files/capacities.csv",capacityData,fieldname[6])
        row['preference']='0'
        row['course']='NA'
        row['cid']='NA'
        row['A']='-1'
        row['B']='-1'
        row['C']='-1'
        


def algo2(studentData):
    c=0
    for i in studentData:
        if i['course']!="NA" and i['cid']!="NA" and i["payment"]=="0":
            modifyStudent_Cap(i)
            c+=1
    print("algo2_count_for_removing_unpdate_capacity_file:",c)
    fileProcess.writeData(path[0],studentData,fieldname[0])
    algo1(studentData)
    ct=0                                   
    for i in studentData:
        if i['course']!="NA":
            print(f"{i['fno']}  {i['name']}  {i['course']}  {i['preference']}")   
            ct+=1
    print(ct)


# a=algo1(studentData) 
# fileProcess.writeData(path[0],a,fieldname[0])
# c=0                                   
# for i in a:
#     if i['course']!="NA":
#         print(f"{i['fno']}  {i['name']}  {i['course']}  {i['preference']}")   
#         c+=1
# print(c)

algo2(studentData)
fileProcess.writeData(path[0],studentData,fieldname[0])