from os import system
import fileProcess
import algorithm

class Admin:
    def __init__(self):
        self.fieldname=[['fno','name','A','B','C','degree','percentage','preference','course','cid','payment','cReport','prn'],
        ['id','name','fees','section'],['name','degree','percentage'],['degree'],['id','name','address','coordinator','password'],
        ['fno','pno','course','cid'],['cid','cname','capacity','fcapacity']]
        
        self.path=["./Modified_Data_Files/students.csv","./Modified_Data_Files/courses.csv","./Modified_Data_Files/eligibilities.csv",
              "./Modified_Data_Files/degrees.txt","./Modified_Data_Files/centers.csv", "./Modified_Data_Files/preferences.csv",
              "./Modified_Data_Files/capacities.csv" ]
        self.studentData=fileProcess.getData(self.path[0],self.fieldname[0])
        self.courseData=fileProcess.getData(self.path[1],self.fieldname[1])
        self.eligibleData=fileProcess.getData(self.path[2],self.fieldname[2])
        self.degreesData=fileProcess.getData(self.path[3],self.fieldname[3])
        self.centersData=fileProcess.getData(self.path[4],self.fieldname[4])
        self.preferenceData=fileProcess.getData(self.path[5],self.fieldname[5])
        self.capacityData=fileProcess.getData(self.path[6],self.fieldname[6])

    def validate(self,uname,passwd):  #Checking sign in credential
        if uname=="admin" and passwd=="admin":
            return True
        else:
            return False

    def course_eligi(self):

        for i in range(1,len(self.courseData)):
            print("------------------------------------------------------------------------------\n")
            #print(f"{self.courseData[0][0]:<10}  {self.eligibleData[0][0]:<10}  \t{self.eligibleData[0][2]:>10}")
            for j in range(1,len(self.eligibleData)):
                if self.courseData[i]['name']==self.eligibleData[j]['name']:
                    print(f"{self.courseData[i]['name']:<10}  {self.eligibleData[j]['degree']:<10}  \t{self.eligibleData[j]['percentage']:>10}")
    

    def center_cap(self):
        for i in range(0,len(self.centersData)):
            print("-------------------------------------------------------------------------------\n")
            #print(f"{self.centersData[0][0]:<10}  {self.centersData[0][1]:<10}  \t{self.capacityData[0][1]:>10}  {self.capacityData[0][2]:>7}  {self.capacityData[0][3]:>7}")
            for j in range (0,len(self.capacityData)):
                if self.centersData[i]['id']==self.capacityData[j]['cid']:
                    print(f"{self.centersData[i]['id']:<10}  {self.centersData[i]['name']:<10}  {self.capacityData[j]['cname']:>10}  {self.capacityData[j]['capacity']:>7}  {self.capacityData[j]['fcapacity']:>7}")

    def student_list(self):
        print("------------------------------------------------------------------------------------\n")
        #print(f"{self.studentData[0][0]:<10}  {self.studentData[0][1]:<10}  {self.studentData[0][2]:>20}  {self.studentData[0][3]:>10}  {self.studentData[0][4]:>10}  {self.studentData[0][10]:>10}  {self.studentData[0][12]:>10}")
        for i in range(0,len(self.studentData)):
            print(f"{self.studentData[i]['fno']:<10}  {self.studentData[i]['name']:<10}  {self.studentData[i]['A']:>20}  {self.studentData[i]['B']:>10}  {self.studentData[i]['C']:>10}  {self.studentData[i]['payment']:>10}  {self.studentData[i]['prn']:>10}")

    def allocate_center1(self):
        system("clear")
        c=0
        self.studentData=algorithm.round1(self.studentData)
        for i in self.studentData:
            if i['course']!="NA":
                c+=1
                print(f"{i['fno']}  {i['name']}  {i['course']}  {i['cid']}  {i['preference']}")
        print("count: ",c)
        fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])

    def allocate_center2(self):
        system("clear")
        c=0
        self.studentData=algorithm.round2(self.studentData)
        for i in self.studentData:
            if i['course']!="NA":
                c+=1
                print(f"{i['fno']}  {i['name']}  {i['course']}  {i['cid']}  {i['preference']}")
        print("count: ",c)
        fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])

    def allocation_list(self):
        count=0
        self.studentData=sorted(self.studentData,key=lambda x:x['course'])
        self.studentData=sorted(self.studentData,key=lambda x:x['cid'])
        self.studentData=sorted(self.studentData,key=lambda x:x['name'])
        for stud in self.studentData:
            if stud["course"]!="NA" and stud["cid"]!="NA":
                 print(f"{stud['fno']:<10}  {stud['name']:<10}  {stud['course']:<10}  {stud['cid']:<10}  {stud['preference']:<10}")
                 count+=1
        print("total student: ",count)
    
    def paid_list(self):
        count=0
        print("")
        self.studentData=sorted(self.studentData,key=lambda x:x['course'])
        self.studentData=sorted(self.studentData,key=lambda x:x['cid'])
        self.studentData=sorted(self.studentData,key=lambda x:x['name'])
        self.studentData=sorted(self.studentData,key=lambda x:x['payment'])
        for stud in self.studentData:
            if stud["course"]!="NA" and stud["cid"]!="NA" and float(stud["payment"])>0:
                 print(f"{stud['fno']:<10}  {stud['name']:<10}  {stud['course']:<10}  {stud['cid']:<10}  {stud['payment']:<10}")
                 count+=1
        print("total student: ",count)
    
    def reported_students(self):
        self.studentData=sorted(self.studentData,key=lambda x:x['course'])
        self.studentData=sorted(self.studentData,key=lambda x:x['cid'])
        self.studentData=sorted(self.studentData,key=lambda x:x['name'])
        print("********************** Students***********************\n")
        space=" "
        print(f"fno{space*10}   name{space*10}   course{space*10}   preference{space*10}    payment{space*10} ")
        for stud in self.studentData:
            if stud['cReport']=='1':
                print(f"{stud['fno']:10}   {stud['name']:20}   {stud['course']:20}   {stud['preference']:20}    {stud['payment']:20}")
    
    def allocate_prn(self):
        #prn allocated on basis of course and center-id and student name.
        c=sorted(self.studentData,key=lambda x:x['course'])
        student=sorted(c,key=lambda x:x['cid'])
        student=sorted(student,key=lambda x:x['name'])
        num=1
        for stud in student:
            if stud['cReport']=='1'and float(stud['payment'])>0:
                stud['prn']=stud['course']+"_"+stud['cid']+"_"+str(num)
                num=num+1
        fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])
        for stud in student:
            if stud['cReport']=='1':
                print(f"{stud['fno']:10}   {stud['name']:20}   {stud['course']:20}   {stud['cid']:20}    {stud['payment']:20}    {stud['prn']:20}")   


    def admitted_Student_List(self):  
        course=str.upper(input("Enter course name (e.g PG-DAC): "))
        cid=str.upper(input("Enter cid: "))

        self.studentData=sorted(self.studentData,key=lambda x:x['course'])
        self.studentData=sorted(self.studentData,key=lambda x:x['cid'])
        for stud in self.studentData:
            if stud['cReport']=='1' and stud['prn']!="NA" and stud['course']==course and stud['cid']==cid:
                print(f"{stud['fno']:10}   {stud['name']:20}   {stud['course']:20}   {stud['cid']:20}    {stud['payment']:20}    {stud['prn']:20}") 

    def update_rank(self):#depend on newly registered student
        system("clear")
        print("\n1.Give Ranks to newly registered students\n2.Modify Existing student's Rank\nEnter choice: ")
        ip=int(input())
        if ip==1:
            print("\nfno  name")
            for stud in self.studentData:
                if stud['A']=='-1'and stud['B']=='-1'and stud['C']=='-1': 
                    print(f"{stud['fno']}   {stud['name']}")
                    sec=int (input("\n1)A \n2)B \n3)C \nEnter section: "))
                    c=0
                    while(sec):
                        l=['A','B','C']
                        while(True):
                            rank=input(f"Enter rank for {l[c]} section:")
                            flag=1
                            for check in self.studentData:
                                if check[l[c]]==rank:
                                    flag=0
                                    break
                            if flag:
                                stud[l[c]]=rank
                                c=c+1 
                                break
                            else:
                                print(f"rank already present in section{l[c]}...please re-enter properly")
                        sec=sec-1      
            fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])
            # for stud in self.studentData:
            #     print(f"{stud['fno']}   {stud['name']}   {stud['A']}    {stud['B']}   {stud['C']}")
        elif ip==2:
            while True:
                print("Enter form number: ")
                fno=input()
                print("Enter Section [A/B/C]: ")
                sec=str.upper(input())
                print("Enter new rank:")
                newRank=input()
                flag=0
                for stud in self.studentData:
                    if stud['fno']==fno:
                        ct=1
                        for check in self.studentData:      #checking whether given rank is already present or not
                                if check[sec]==newRank:
                                    ct=0
                                    break
                        if ct==1:
                             stud[sec]=newRank
                        else:
                            print("rank is already present")
                        flag=1
                        break
                    else:
                        flag=0
                if flag==1:
                    print("Rank updated successfully")
                    break
                else:
                    print("Student doesn't exist")
                e=str.upper(input("do you want to continue [Y/N]:"))
                if e=="N":
                    break
        fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])
        


