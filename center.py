from os import system
import fileProcess

class Center:
    def __init__(self):
        self.fieldname=[['fno','name','A','B','C','degree','percentage','preference','course','cid','payment','cReport','prn'],
        ['id','name','fees','section'],['name','degree','percentage'],['degree'],['id','name','address','coordinator','password'],
        ['fno','pno','course','cid'],['cid','cname','capacity','fcapacity']]
        
        self.path=["./Modified_Data_Files/students.csv","./Modified_Data_Files/courses.csv","./Modified_Data_Files/eligibilities.csv",
              "./Modified_Data_Files/degrees.txt","./Modified_Data_Files/centers.csv", "./Modified_Data_Files/preferences.csv",
              "./Modified_Data_Files/capacities.csv" ]
        self.studentData=fileProcess.getData(self.path[0],self.fieldname[0])
        self.courseData=fileProcess.getData(self.path[1],self.fieldname[1])
        self.eligibleData=fileProcess.getData(self.path[2],self.fieldname[2])
        self.degreesData=fileProcess.getData(self.path[3],self.fieldname[3])
        self.centersData=fileProcess.getData(self.path[4],self.fieldname[4])
        self.preferenceData=fileProcess.getData(self.path[5],self.fieldname[5])
        self.capacityData=fileProcess.getData(self.path[6],self.fieldname[6])

    def validate(self,uname,passwd):
        flag=0
        for i in range(0,len(self.centersData)):
            if uname==self.centersData[i]['id'] and passwd==self.centersData[i]['password']:
                return True 
            else:
                flag=0
        if flag==0:
            return False
    
    def list_course(self,cid):
        print("**********Available Courses****************")
        for course in self.capacityData:
            if course['cid']==cid:
                print(f"{course['cname']}")
        #print(self.capacityData)
    
    def list_student(self,cid):
        cname=str.upper(input("Enter Course Name: "))
        print("**********************Allocated Students***********************\n")
        
        # for stud in self.studentData:
        #     if stud['cid']==cid and float(stud['payment'])>0:
        #         print(f"{stud['fno']:10}   {stud['name']:20}   {stud['course']:20}   {stud['preference']:20}   {stud['payment']}")
        s=sorted(self.studentData,key=lambda x:x['name']) #sorted by name
        for i in s:
            if i['cid']==cid and i['course']==cname:
                print(f"{i['fno']:10}   {i['name']:20}   {i['course']:20}   {i['preference']:20}")

    def update_report_status(self,cid):
        print("Enter a form number of student[status will update to 1]:")
        fno=input()
        status=input("Enter Status [0 or 1 ]: ")
        flag=0
        for i in self.studentData:
            if cid==i['cid'] and float(i['payment'])>0 and i["fno"]==fno:
                i["cReport"]=status
                flag=1
                break
            else:
                flag=0
        if flag:
            fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])
            print("reporting status of ",fno," is updated successfully")
        else:
            print("student is not allocated to this center")

    def admitted_Student_List(self,cid):
        course=str.upper(input("Enter Course Name : "))
        student=sorted(self.studentData,key=lambda x:x['name'])
        for stud in student:
            if stud['cReport']=='1' and stud['prn']!="NA" and stud['cid']==cid and stud['course']==course:
                print(f"{stud['fno']:10}   {stud['name']:20}   {stud['course']:20}   {stud['cid']:20}    {stud['payment']:20}    {stud['prn']:20}") 