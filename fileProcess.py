import csv

def getData(path,fnames):
    with open(path,"r") as csv_file:
        csv_reader=csv.DictReader(csv_file,fieldnames=fnames)
        data=[]
        #data.append(csv_reader.fieldnames)
        for row in csv_reader:
            data.append(row)
        return data
      

def writeData(path,data,fnames):
    with open(path,"w")as csv_file:
        csv_writer=csv.DictWriter(csv_file,fieldnames=fnames)

        #csv_writer.writeheader()#write fieldname as a first line
        for line in data:
            csv_writer.writerow(line)
    print("success")