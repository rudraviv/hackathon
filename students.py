from os import system
#import csv
import fileProcess
from collections import OrderedDict 

class Student:
   
    def __init__(self):
        self.fieldname=[['fno','name','A','B','C','degree','percentage','preference','course','cid','payment','cReport','prn'],
        ['id','name','fees','section'],['name','degree','percentage'],['degree'],['id','name','address','coordinator','password'],
        ['fno','pno','course','cid'],['cid','cname','capacity','fcapacity']]
        
        self.path=["./Modified_Data_Files/students.csv","./Modified_Data_Files/courses.csv","./Modified_Data_Files/eligibilities.csv",
              "./Modified_Data_Files/degrees.txt","./Modified_Data_Files/centers.csv", "./Modified_Data_Files/preferences.csv",
              "./Modified_Data_Files/capacities.csv" ]
        self.studentData=fileProcess.getData(self.path[0],self.fieldname[0])
        self.courseData=fileProcess.getData(self.path[1],self.fieldname[1])
        self.eligibleData=fileProcess.getData(self.path[2],self.fieldname[2])
        self.degreesData=fileProcess.getData(self.path[3],self.fieldname[3])
        self.centersData=fileProcess.getData(self.path[4],self.fieldname[4])
        self.preferenceData=fileProcess.getData(self.path[5],self.fieldname[5])
        self.capacityData=fileProcess.getData(self.path[6],self.fieldname[6])
        
    def register(self):
        system("clear")
        name=input("Enter Name:")
        c=0
        degree="NA"
        print("\n")
        print("0)None")
        for deg in range(4,len(self.degreesData)):          #start from 5th degree
            c+=1                                            #to print sequence
            print(f"{c}){self.degreesData[deg]['degree']}")
        n=int(input("Enter Degree number you have : "))
        degree=self.degreesData[(n+4)-1]['degree']
        if(n==0):                                       #if degree not enlisted

            print("You are not eligible for CDAC")
        
        else:
            per=float(input("Enter graduation percentage:"))
            if per>=50:                                 #check percentage>50
                fno=len(self.studentData)+1             #calculate form number.
                d={'fno':str(fno),'name':str(name),'A':'-1','B':'-1','C':'-1','degree':str(degree),'percentage':str(per),'preference':'0','course':'NA','cid':'NA','payment':'0.0','cReport':'0','prn':'NA'}
                self.studentData.extend([d])
        fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])      #writing into student.csv
        ct=0
        for i in range(0,len(self.studentData)):
            ct+=1
            print(f"{self.studentData[i]['fno']:<10}  {self.studentData[i]['name']:<10}  {self.studentData[i]['A']:>20}  {self.studentData[i]['B']:>10}  {self.studentData[i]['C']:>10}  {self.studentData[i]['payment']:>10}  {self.studentData[i]['prn']:>10}")
        print("count:",ct)


    def validate(self,uname,passwd):    #checking student existance 
        #system("clear")
        flag=0
        global fno
        fno=int(uname)
        #studentData=fileProcess.getData()
        for i in range(0,len(self.studentData)):
            if self.studentData[i]['fno']==uname and passwd ==self.studentData[i]['name']:
                return True
            else:
                flag=0
        if flag==0:
            return False
            
    def courseList(self):
        c=[]
        system("clear")
        count=1
        print("\n**********Eligible courses******************\n")
        for stud in self.studentData:
            if stud['fno']==str(fno):               #comparing form number in student.csv
                for i in range(0,len(self.eligibleData)):       
                    if stud['degree']==self.eligibleData[i]['degree']:      #checking eligibility
                        c.append((self.eligibleData[i]['name']))
                        print(f"\t{count}){self.eligibleData[i]['name']}")
                        count+=1
        # print(self.studentData)
   
    def centerList(self):
        print("\n*********Centers**************\n")
        for i in range(0,len(self.centersData)):            #printing centers.csv
            print(f"{self.centersData[i]['name']}\t\t{ self.centersData[i]['address'] }")


    def setPreference(self,uname):      #student must give all preferences once. 
        sections=['A','B','C']
        a=False
        for pref in self.preferenceData:    #check whether preference already given or not
            if pref['fno']!=uname:
                a=True
            else:
                a=False
                break
        if a:
            c=1 #for printing sequence 
            l=[]        #this list is for eligible course according to ranks for all sections
            for sec in sections:        #section-wise
                for stud in self.studentData:
                    if stud['fno']==uname :
                        if stud[sec]!='-1':     #check student rank for particular section is not -1
                            for course in self.courseData:  
                                if course['section']==sec:
                                    for elig in self.eligibleData:
                                        if elig['name']==course['name']and float(stud['percentage'])>float(elig['percentage']):
                                            for center in self.capacityData:
                                                if course['name']==center['cname']:
                                                    # print(f"{c}){course['name']}    {center['cid']}")
                                                    l.append({c:course['name']+" "+center['cid']})
                                                    c+=1
                                            break
            c=1 #for printing sequence
            for i in l:
                print(f"{c}){i[c]}")    #printing available courses and its center id
                c+=1
            count=1  # count is for to check preference numbers upto 10
            while True:
                print("choose course and corresponding center:")
                ch=int(input())
                if count==1:                        #this is for student's first preference .
                    s=l[ch-1][ch]
                    temp=OrderedDict()        #ordered dict
                    temp['fno']=str(uname)
                    temp['pno']=str(count)
                    temp['course']=s[0:s.index(" ")]    #storing course by applying string slicing
                    temp['cid']=s[s.index(" ")+1:]      
                    self.preferenceData.insert(len(self.preferenceData),temp)
                    
                    #count=count+1
                    print("current count:",count)
                    fileProcess.writeData(self.path[5],self.preferenceData,self.fieldname[5])
                else:                                   
                    s=l[ch-1][ch]
                    for pref in self.preferenceData:        #checking whether students preference is already present
                        if pref['fno']==uname and pref['course']==s[0:s.index(" ")] and pref['cid']==['cid']==s[s.index(" ")+1:]:
                            print("this preference already present")
                            count=count-1
                            break
                        else:
                            temp=OrderedDict()        #ordered dict
                            temp['fno']=str(uname)
                            temp['pno']=str(count)
                            temp['course']=s[0:s.index(" ")]
                            temp['cid']=s[s.index(" ")+1:]
                            self.preferenceData.insert(len(self.preferenceData),temp)
                           # count=count+1
                            fileProcess.writeData(self.path[5],self.preferenceData,self.fieldname[5])
                            break
                print("Do you want to add more?(Y/N):")
                y=str.upper(input())
                if y=="N" or count==10:
                    break
                else:
                    count+=1
        else:
            print("you already given a preferences")            

    def checkAllocatedCenter(self,fno):
        flag=0
        for i in self.studentData:      #check whether center is allocated or not
            if i['fno']==fno and i['course']!="NA":
                print(f"\n{i['fno']}  {i['name']}  {i['course']}  {i['cid']}")
                flag=0
                break
            else:
                flag=1
        if flag==1:
            print(".....Center Not Allocated.....\n")
                

    def updatePaymentDetail(self,fno):
        for stud in self.studentData:
            if stud['fno']==fno:
                print(f"\nPaid Fees:{stud['payment']}")
                if stud['payment']=='11800':
                    print("Do you want to update Second Installment Payment Status (Y/N)")
                    a=str.upper(input())
                    if a=='Y':
                        for course in self.courseData:
                            if stud['course']==course['name']:
                                stud['payment']=course['fees']
                                fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])
                                print("Updated")
                    else:
                        break
                elif stud['payment']=='0':
                    print("Do you want to update First Installment Payment Status (Y/N)")
                    a=str.upper(input())
                    if a=='Y':
                        stud['payment']='11800'
                        fileProcess.writeData(self.path[0],self.studentData,self.fieldname[0])
                        print("Updated")
                    else:
                        break
                else:
                    print("\nYou already paid full fees")