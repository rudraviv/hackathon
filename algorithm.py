import fileProcess

fieldname=[['fno','name','A','B','C','degree','percentage','preference','course','cid','payment','cReport','prn'],
['id','name','fees','section'],['name','degree','percentage'],['degree'],['id','name','address','coordinator','password'],
['fno','pno','course','cid'],['cid','cname','capacity','fcapacity']]
path=["./Modified_Data_Files/students.csv","./Modified_Data_Files/courses.csv","./Modified_Data_Files/eligibilities.csv",
              "./Modified_Data_Files/degrees.txt","./Modified_Data_Files/centers.csv", "./Modified_Data_Files/preferences.csv",
              "./Modified_Data_Files/capacities.csv" ]
#studentData=fileProcess.getData(path[0],fieldname[0])
courseData=fileProcess.getData(path[1],fieldname[1])
eligibleData=fileProcess.getData(path[2],fieldname[2])
degreesData=fileProcess.getData(path[3],fieldname[3])
centersData=fileProcess.getData(path[4],fieldname[4])
preferenceData=fileProcess.getData(path[5],fieldname[5])
capacityData=fileProcess.getData(path[6],fieldname[6])

def round1(studentData):#get from admin
    sections=['A','B','C']
    count=0
    for cycle in range(1,11):  #allocation upto 10 preferences
        for rank in sections:   #loop for every rank
            studentData=sorted(studentData,key=lambda x:int(x[rank]))
            for stud in studentData:    
                if stud[rank]!='-1' and stud['course']=="NA" and stud['preference']=='0':  #check whether already course /center allocated or not
                    for pref in preferenceData:
                        if pref['pno']==str(cycle) and pref['fno']==stud['fno']:        #check whether selected fno present in preference list and is preference match with cycle.
                            for c in courseData:
                                if rank==c['section'] and c['name']==pref['course']:    #check section present in section of course and check course match with selected course of preference
                                    for cap in capacityData:
                                        if cap['cid'] == pref['cid'] and cap['cname'] == pref['course']:    #check capacity.
                                            if int(cap['fcapacity'])<int(cap["capacity"]):
                                                    stud['course']=pref['course']
                                                    stud['preference']=pref['pno']
                                                    stud['cid']=pref['cid']
                                                    cap['fcapacity'] = str(int(cap['fcapacity']) +1)    #update filled capacity
                                                    count+=1
                                                    #print(f"{stud['fno']}  {stud['name']}  {stud['course']}  {stud['preference']}  {stud['cid']}")
    fileProcess.writeData(path[6],capacityData,fieldname[6])
    print("algo1 count:",count)
    return studentData

def round2(studentData):#get from admin
    c=0
    for stud in studentData:
        if stud['course']!="NA" and stud['cid']!="NA" and stud["payment"]=="0":
            for i in capacityData:
                if stud['course']==i['cname'] and stud['cid']==i['cid']:
                    if int(i['fcapacity'])>0:
                        i['fcapacity']=str(int(i['fcapacity']) -1)
                        stud['preference']='-1'
                        stud['payment']='-1'
                        
            fileProcess.writeData("./Modified_Data_Files/capacities.csv",capacityData,fieldname[6])
            
            c+=1
            print(f"{stud['fno']}  {stud['name']}   {stud['preference']}   {stud['course']}")
    print("removed students: ",c,"\n",)
    #print(capacityData)
    
    for s in studentData:
        if s['preference'] != '0':
            s['course'] = 'NA'
            s['cid'] = 'NA'
            s['preference'] = '0'
    
    for cap in capacityData:
        cap['fcapacity'] = 0


    sections=['A','B','C']
    count=0
    for cycle in range(1,11):
        for rank in sections:
            studentData=sorted(studentData,key=lambda x:int(x[rank]))
            for stud in studentData:
                if int(stud[rank])>0 and stud['preference']=='0'and stud['payment']!='-1':
                    for pref in preferenceData:
                        if pref['pno']==str(cycle) and pref['fno']==stud['fno']:
                            for c in courseData:
                                if rank==c['section'] and c['name']==pref['course']:
                                    for cap in capacityData:
                                        if cap['cid'] == pref['cid'] and cap['cname'] == pref['course']:
                                            if int(cap['fcapacity'])<int(cap["capacity"]):
                                                    stud['course']=pref['course']
                                                    stud['preference']=pref['pno']
                                                    stud['cid']=pref['cid']
                                                    cap['fcapacity'] = str(int(cap['fcapacity']) +1)
                                                    count+=1
                                                   # print(f"{stud['fno']}  {stud['name']}  {stud['course']}  {stud['preference']}  {stud['cid']}")
    #print("round 2_count:",count)
    return studentData