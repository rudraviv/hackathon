import fileProcess


fieldname=[['fno','name','A','B','C','degree','percentage','preference','course','cid','payment','cReport','prn'],
['id','name','fees','section'],['name','degree','percentage'],['degree'],['id','name','address','coordinator','password'],
['fno','pno','course','cid'],['cid','cname','capacity','fcapacity']]
path=["./Modified_Data_Files/students.csv","./Modified_Data_Files/courses.csv","./Modified_Data_Files/eligibilities.csv",
              "./Modified_Data_Files/degrees.txt","./Modified_Data_Files/centers.csv", "./Modified_Data_Files/preferences.csv",
              "./Modified_Data_Files/capacities.csv" ]
studentData=fileProcess.getData(path[0],fieldname[0])
courseData=fileProcess.getData(path[1],fieldname[1])
eligibleData=fileProcess.getData(path[2],fieldname[2])
degreesData=fileProcess.getData(path[3],fieldname[3])
centersData=fileProcess.getData(path[4],fieldname[4])
preferenceData=fileProcess.getData(path[5],fieldname[5])
capacityData=fileProcess.getData(path[6],fieldname[6])


def algo2(studentData):
    c=0
    for stud in studentData:
        if stud['course']!="NA" and stud['cid']!="NA" and stud["payment"]=="0":
            for i in capacityData:
                if stud['course']==i['cname'] and stud['cid']==i['cid']:
                    if int(i['fcapacity'])>0:
                        i['fcapacity']=str(int(i['fcapacity']) -1)
                        stud['preference']='-1'
                        stud['payment']='-1'
                        
            #fileProcess.writeData("./Modified_Data_Files/capacities.csv",capacityData,fieldname[6])
            
            c+=1
            print(f"{stud['fno']}  {stud['name']}   {stud['preference']}   {stud['course']}")
    print("removed students: ",c,"\n",)
    print(capacityData)
    
    for s in studentData:
        if s['preference'] != '0':
            s['course'] = 'NA'
            s['cid'] = 'NA'
            s['preference'] = '0'
    
    for cap in capacityData:
        cap['fcapacity'] = 0


    sections=['A','B','C']
    count=0
    for cycle in range(1,11):
        for rank in sections:
            studentData=sorted(studentData,key=lambda x:int(x[rank]))
            for stud in studentData:
                if int(stud[rank])>0 and stud['preference']=='0'and stud['payment']!='-1':
                    for pref in preferenceData:
                        if pref['pno']==str(cycle) and pref['fno']==stud['fno']:
                            for c in courseData:
                                if rank==c['section'] and c['name']==pref['course']:
                                    for cap in capacityData:
                                        if cap['cid'] == pref['cid'] and cap['cname'] == pref['course']:
                                            if int(cap['fcapacity'])<int(cap["capacity"]):
                                                    stud['course']=pref['course']
                                                    stud['preference']=pref['pno']
                                                    stud['cid']=pref['cid']
                                                    cap['fcapacity'] = str(int(cap['fcapacity']) +1)
                                    #incrementor(capacityData,stud['cid'],stud['course'],stud['preference'])
                                                    count+=1
                                                    print(f"{stud['fno']}  {stud['name']}  {stud['course']}  {stud['preference']}  {stud['cid']}")
    print("algo1:",count)
    
algo2(studentData)
#fileProcess.writeData(path[0],studentData,fieldname[0])
final=0
for i in studentData:
    if i['course']!="NA":
        print(f"{i['fno']}  {i['name']}  {i['course']}  {i['preference']}  {i['cid']}")
        final+=1
print("final:",final)